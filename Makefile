# This file is part of purgedir.
# Copyright (C) 2024 Sergey Poznyakoff
#
# Purgedir is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Purgedir is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with purgedir.  If not, see <http://www.gnu.org/licenses/>.

# Installation prefix
PREFIX = /usr
BINDIR = $(PREFIX)/bin
MANDIR = $(PREFIX)/share/man

MAN1DIR = $(MANDIR)/man1

# Install program.  Use cp(1) if not available.
INSTALL = install
# Program to make directory hierarchy.
MKHIER  = install -d

## ################################################################## ## 
## There is nothing to modify below this line                         ##

PACKAGE_NAME = purgedir
PACKAGE_VERSION = 1.0
PACKAGE_TARNAME = $(PACKAGE_NAME)-$(PACKAGE_VERSION)
PACKAGE_BUGREPORT = gray@gnu.org

SOURCES = purgedir.c mem.c ckp.c filematch.c parseopt.c
HEADERS = purgedir.h parseopt.h mem.h version.h
OBJECTS = $(SOURCES:.c=.o)
CFLAGS = -ggdb -Wall
LIBS = -lpthread -lrt -lm
MAN1PAGES=purgedir.1

purgedir: $(OBJECTS)
	$(CC) -opurgedir $(CFLAGS) $(OBJECTS) $(LDFLAGS) $(LIBS)

t/mkhier: t/mkhier.o parseopt.o mem.o
	$(CC) -ot/mkhier $(CFLAGS) t/mkhier.o parseopt.o mem.o -lrt

clean:
	rm -f purgedir $(OBJECTS) t/mkhier

.c.o:
	$(CC) -c $(CFLAGS) $(CPPFLAGS) -o $@ $<

version.h: Makefile
	{\
	  echo '#define PACKAGE_NAME "$(PACKAGE_NAME)"'; \
          echo '#define PACKAGE_VERSION "$(PACKAGE_VERSION)"'; \
          echo '#define PACKAGE_BUGREPORT "$(PACKAGE_BUGREPORT)"'; \
        } > version.h

purgedir.o: purgedir.h parseopt.h mem.h version.h
mem.o: mem.c mem.h parseopt.h
ckp.o: ckp.c purgedir.h parseopt.h mem.h
filematch.o: purgedir.h parseopt.h mem.h
parseopt.o: parseopt.c parseopt.h mem.h
t/mkhier.o: parseopt.h mem.h

TAGS: Makefile $(SOURCES) $(HEADERS)
	etags $(SOURCES) $(HEADERS)

tags: TAGS

# ###################
# Testsuite
# ###################
AUTOM4TE = autom4te
AUTOTEST = $(AUTOM4TE) --language=autotest
TEST_PROGRAMS = t/mkhier

TESTSUITE_AT = \
 t/testsuite.at\
 t/basic.at\
 t/deep.at\
 t/mixed.at\
 t/remdir.at\
 t/keep.at\
 t/dry-run.at\
 t/name.at\
 t/mmin.at\
 t/amin.at\
 t/perm.at\
 t/type.at\
 t/not.at\
 t/or.at\
 t/and.at\
 t/prec.at\
 t/symlinks.at\
 t/permute.at

package.m4: Makefile
	{                                      \
	  echo '# Signature of the current package.'; \
	  echo 'm4_define([AT_PACKAGE_NAME],      [$(PACKAGE_NAME)])'; \
	  echo 'm4_define([AT_PACKAGE_TARNAME],   [$(PACKAGE_TARNAME)])'; \
	  echo 'm4_define([AT_PACKAGE_VERSION],   [$(PACKAGE_VERSION)])'; \
	  echo 'm4_define([AT_PACKAGE_STRING],    [$(PACKAGE_TARNAME)])'; \
	  echo 'm4_define([AT_PACKAGE_BUGREPORT], [$(PACKAGE_BUGREPORT)])'; \
	} > package.m4

EXTRA_DIST = atlocal package.m4 $(TESTSUITE_AT) t/mkhier.c

testsuite: package.m4 $(TESTSUITE_AT)
	$(AUTOTEST) -I . -I t t/testsuite.at -o testsuite

check: purgedir testsuite t/mkhier
	@./testsuite

# ###################
# Installation rules
# ###################
install: install-bin install-man

install-bin: purgedir
	$(MKHIER) $(DESTDIR)$(BINDIR)
	$(INSTALL) purgedir $(DESTDIR)$(BINDIR)

install-man:
	$(MKHIER) $(DESTDIR)$(MAN1DIR)
	$(INSTALL) $(MAN1PAGES) $(DESTDIR)$(MAN1DIR)

# ###################
# Distribution rules
# ###################
DOCS = README NEWS
DISTDIR   = $(PACKAGE_NAME)-$(PACKAGE_VERSION)
DISTFILES = $(SOURCES) $(HEADERS) $(MAN1PAGES) $(EXTRA_DIST) $(DOCS) Makefile

distdir:
	rm -rf $(DISTDIR) && \
	mkdir $(DISTDIR) &&  \
	tar cf - $(DISTFILES) | tar -C $(DISTDIR) -xf -

dist: distdir
	tar cfz $(DISTDIR).tar.gz $(DISTDIR)
	rm -rf $(DISTDIR)

distcheck: dist
	tar xfz $(DISTDIR).tar.gz
	@if $(MAKE) -C $(DISTDIR) $(DISTCHECKFLAGS) check; then \
	  text="$(DISTDIR).tar.gz ready for distribution"; \
          echo "$$text" | sed s/./=/g; \
          echo "$$text"; \
          echo "$$text" | sed s/./=/g; \
	  rm -rf $(DISTDIR); \
        else \
          exit 2; \
        fi
