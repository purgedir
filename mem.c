/* This file is part of purgedir.
   Copyright (C) 2024 Sergey Poznyakoff

   Purgedir is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Purgedir is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with purgedir.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <string.h>
#include "parseopt.h"

void
alloc_die(void)
{
	error(1, 0, "out of memory");
}

void *
xmalloc(size_t size)
{
	char *p = malloc(size);
	if (!p)
		alloc_die();
	return p;
}

void *
xcalloc(size_t nmemb, size_t size)
{
	char *p = calloc(nmemb, size);
	if (!p)
		alloc_die();
	return p;
}

char *
xstrdup(char const *str)
{
	char *p = strdup(str);
	if (!p)
		alloc_die();
	return p;
}

void *
xrealloc(void *ptr, size_t size)
{
	char *p = realloc(ptr, size);
	if (!p)
		alloc_die();
	return p;
}

void *
x2nrealloc(void *p, size_t *pn, size_t s)
{
	size_t n = *pn;
	char *newp;

	if (!p) {
		if (!n) {
			/* The approximate size to use for initial small
			   allocation requests, when the invoking code
			   specifies an old size of zero.  64 bytes is the
			   largest "small" request for the GNU C library
			   malloc.  */
			enum { DEFAULT_MXFAST = 64 };

			n = DEFAULT_MXFAST / s;
			n += !n;
		}
	} else {
		/* Set N = ceil (1.5 * N) so that progress is made if N == 1.
		   Check for overflow, so that N * S stays in size_t range.
		   The check is slightly conservative, but an exact check isn't
		   worth the trouble.  */
		if ((size_t) -1 / 3 * 2 / s <= n)
			alloc_die();
		n += (n + 1) / 2;
	}

	newp = xrealloc(p, n * s);
	*pn = n;
	return newp;
}

static size_t
baselen(char const *dir)
{
	size_t len = strlen(dir);
	while (len > 0 && dir[len-1] == '/')
		len--;
	return len;
}

char *
xcatfile(char const *dir, char const *name)
{
	size_t size = 0;
	size_t dirlen = 0;
	size_t namelen;
	char *file;

	if (dir) {
		dirlen = baselen(dir);
	}

	namelen = baselen(name);
	size = dirlen + namelen + 2;
	file = xmalloc(size);
	if (dir) {
		memcpy(file, dir, dirlen);
		file[dirlen] = '/';
		dirlen++;
	}
	memcpy(file + dirlen, name, namelen);
	file[dirlen+namelen] = 0;

	return file;
}
