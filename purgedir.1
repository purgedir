.\" This file is part of purgedir -*- nroff -*-
.\" Copyright (C) 2024 Sergey Poznyakoff
.\"
.\" Purgedir is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Purgedir is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with purgedir.  If not, see <http://www.gnu.org/licenses/>.
.\"
.TH PURGEDIR 1 "March 6, 2024" "PURGEDIR"
.SH NAME
purgedir \- fast removal of directory contents
.SH SYNOPSIS
\fBpurgedir\fR\
 [\fB\-d\fI N\fR]\
 [\fB\-i\fI T\fR]\
 [\fB\-l\fI MIN\fR[\fB\-\fIMAX\fR]]\
 [\fB\-n\fR]\
 [\fB\-r\fR]\
 [\fB\-v\fR]\
 [\fB\-amin\fR [\fB+\-\fR]\fIN]\fR]\
 [\fB\-atime\fR [\fB+\-\fR]\fIN]\fR]\
 [\fB\-cmin\fR [\fB+\-\fR]\fIN]\fR]\
 [\fB\-ctime\fR [\fB+\-\fR]\fIN]\fR]\
 [\fB\-daystart\fR]\
 [\fB\-dry\-run\fR]\
 [\fB\-gid\fI N\fR]\
 [\fB\-keep\fR]\
 [\fB\-la-interval\fI T\fR]\
 [\fB\-la\-range\fI MIN\fR[\fB\-\fIMAX\fR]]\
 [\fB\-max\-dir\fI N]\
 [\fB\-mmin\fR [\fB+\-\fR]\fIN]\fR]\
 [\fB\-mtime\fR [\fB+\-\fR]\fIN]\fR]\
 [\fB\-name\fI PATTERN\fR]\
 [\fB\-not\fR]\
 [\fB\-or\fR]\
 [\fB\-perm\fR [\fB\-/\fR]\fIMODE\fR]\
 [\fB\-remove\-dir\fR]\
 [\fB\-type\fR \fBb\fR|\fBc\fR|\fBp\fR|\fBf\fR|\fBl\fR|\fBs\fR]\
 [\fB\-uid\fI N\fR]\
 [\fB\-verbose\fR]\
 \fIDIR\fR...
.sp
\fBpurgedir\fR\
 [\fB\-V\fR]\
 [\fB\-help\fR]\
 [\fB\-version\fR]
.SH DESCRIPTION
.B Purgedir
recursively removes all files and subdirectories from the
supplied directories.  The program is optimized for quick removal of
a very large number of files on \fBext3\fR and \fBext4\fR filesystems.
However, it performs better than the standard \fBrm\fR on other
filesystems as well, in particular on \fBZFS\fR and \fBBtrFS\fR.
.PP
For each directory argument (\fIDIR\fR), the program will recursively
remove all files and directories in it, but will retain the directory
itself (unless given the \fB\-r\fR option, see below).
.PP
Symbolic links occuring within directories are removed, but not
followed.  Symbolic links as arguments are not accepted, even if
they point to a directory.
.PP
Additional options are provided to ensure that the load average of the
server remains within the given limits.  A set of \fBfind\fR-like
predicate is provided to select the files to be removed.
.SH OPTIONS
Command line syntax is similar to that of
.BR find (1).
All options begin with a single dash.  Single-character options cannot
be clustered (i.e., use \fB-n -v\fR, instead of \fB-nv\fR).  Options
and non-option arguments can be interspersed, that is, both
.sp
.EX
purgedir -v dir
.EE
.sp
and
.sp
.EX
purgedir dir -v
.EE
.sp
mean the same.
.SS General-purpose options
.TP
\fB\-d\fR, \fB\-max\-dir\fI N
Limit number of simultaneously open directories to \fIN\fR (default:
16).
.TP
\fB\-n\fR, \fB\-dry\-run\fR
Dry run mode: do nothing, but print what would have been done instead.
.TP
\fB\-r, \fB\-remove\-dir\fR
Remove the top-level directories after purging them (similar to \fBrm -r\fR).
.TP
\fB\-keep\fR
Keep directory hierarchy: don't remove subdirectories even if empty.
.TP
\fB\-v\fR, \fB\-verbose\fR
Increase verbosity.
.SS Load Average Control
Load average control is an optional feature which allows you to avoid
peaks in the system LA during purging.  It is enabled when given the
\fB\-la\-range\fR option:
.TP
\fB\-l\fR, \fB\-la\-range \fIMIN\fR[\fB\-\fIMAX\fR]
Throttle if LA reaches \fIMAX\fR and resume when it falls below
\fIMIN\fR.  Both \fIMIN\fR and \fIMAX\fR can be integer or
floating-point numbers.
.TP
\fB\-i\fR, \fB\-la\-interval\fI T\fR
Check load average each T seconds (floating point number). Default is
5.000.
.SS Predicates
Predicates allow you to select files for removal.  They are a subset of
.BR find (1)
predicates.
.PP
.TP
\fB\-name\fI PATTERN\fR
Select only files matching
.BR glob (7)
\fIPATTERN\fR.
.TP
\fB\-type b\fR|\fBc\fR|\fBp\fR|\fBf\fR|\fBl\fR|\fBs\fR
Match files of this type: \fBb\fR for block device, \fBc\fR for
character device, \fBp\fR for FIFO, \fBf\fR for regular file, \fBl\fR
(ell) for symlink, and \fBs\fR for socket.
.IP
Notice, that since predicates are applied only to non-directory files,
the \fBd\fR type is deliberately not implemented.
.TP
\fB\-perm\fR [\fB\-/\fR]\fIMODE\fR
Files with matching \fIMODE\fR (octal).  The leading \fB\-\fR requires 
that all of the permission bits be set, and \fB/\fR requires that any
of them be set.  If neither is given, file mode is matched exactly.
.TP
\fB\-uid \fIUID\fR
Files owned by this \fIUID\fR number.
.TP
\fB\-gid \fIGID\fR
Files owned by this \fIGID\fR number.
.PP
The following predicates select files based on their time.  If the
argument starts with \fB+\fR, files older than the specified amount of
time ago are selected.  If it starts with \fB\-\fR, files newer than that amount of time are selected.
.TP
\fI\-amin\fR [\fB+\-\fR]\fIN\fR
Select files by atime in minutes.
.TP
\fI\-atime\fR [\fB+\-\fR]\fIN\fR
Select files by atime in days.
.TP
\fI\-cmin\fR [\fB+\-\fR]\fIN\fR
Select files by ctime in minutes.
.TP
\fI\-atime\fR [\fB+\-\fR]\fIN\fR
Select files by ctime in days.
.TP
\fI\-mmin\fR [\fB+\-\fR]\fIN\fR
Select files by ctime in minutes.
.TP
\fI\-mtime\fR [\fB+\-\fR]\fIN\fR
Select files by mtime in days.
.PP
By default times are measured from 24 hours ago.  This is altered by
the following option:
.TP
.B \-daystart
Measure times from the beginning of today rather than from 24 hours
ago.
.PP
By default, multiple predicates are joined using boolean
.BR AND .
The following predicates modify that:
.TP
.B \-not
Negate the effect of the next predicate.
.TP
.B \-or
Join the two adjacent predicates by logical \fIOR\fR.
.PP
Use parentheses to change predicate precedence. Be sure to quote them,
to prevent them from being expanded by the shell.  E.g.:
.sp
.EX
purgedir dir \\( -perm /111 -name '0?' \\) -or \\( -perm 755 -name '1?' \\)
.EE
.SS Informative options
When one of these options is given, the program prints the requested
piece of information and exits with status 0.
.TP
.B \-help
Display a short help summary.
.TP
\fB\-V\fR, \fB\-version\fR
Display program version and licensing information.
.SH EXIT CODES
.TP
.B 0
Successful termination.
.TP
.B 1
A fatal error occurred.
.TP
.B 2
Some files were not removed.
.TP
.B 3
Command line usage error.
.SH "SEE ALSO"
.BR rm (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray@gnu.org>.
.SH COPYRIGHT
Copyright \(co 2024 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

