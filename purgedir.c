/* This file is part of purgedir.
   Copyright (C) 2024 Sergey Poznyakoff

   Purgedir is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Purgedir is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with purgedir.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include "purgedir.h"
#include "parseopt.h"
#include "version.h"

static int dry_run;
static int remove_top_dir;
static int keep_dirs;
int verbose;
FILECOND_LIST *fm;

static int exit_code = EX_OK;

struct workdir {
	struct workdir *prev, *next;
	struct workdir *parent;
	int fd;
	int toplevel;
	int keep;
	dev_t st_dev;
	ino_t st_ino;
	char *name;
};

struct file_entry {
	struct workdir *dir;
	dev_t st_dev;
	ino_t st_ino;
	char *name;
};

struct dir_list {
	struct workdir *head, *tail;
	size_t count;
};

#define DIR_LIST_INITIALIZER { NULL, NULL, 0 }

static inline size_t
dir_list_count(struct dir_list *list)
{
	return list->count;
}

static void
dir_list_unlink(struct dir_list *list, struct workdir *ent)
{
	struct workdir *p;

	if ((p = ent->prev) != NULL)
		p->next = ent->next;
	else
		list->head = ent->next;

	if ((p = ent->next) != NULL)
		p->prev = ent->prev;
	else
		list->tail = ent->prev;
	list->count--;

	ent->prev = ent->next = NULL;
}

static void
dir_list_link_head(struct dir_list *list, struct workdir *ent)
{
	ent->next = list->head;
	if (list->head)
		list->head->prev = ent;
	else
		list->tail = ent;
	list->head = ent;
	list->count++;
}

static void
dir_list_link_tail(struct dir_list *list, struct workdir *ent)
{
	ent->prev = list->tail;
	if (list->tail)
		list->tail->next = ent;
	else
		list->head = ent;
	list->tail = ent;
	list->count++;
}

static struct dir_list dir_list_head, dir_list_lru;
static size_t open_dir_max = QRM_DIR_MAX;

/* Mark DIR and all its parents as being kept. */
static void
workdir_keep(struct workdir *dir)
{
	do {
		dir->keep = 1;
	} while ((dir = dir->parent) != NULL && !dir->keep);
}

void
workdir_close(struct workdir *dir)
{
	close(dir->fd);
	dir->fd = -1;
}

struct workdir *
workdir_add(struct workdir *dir, char const *name)
{
	struct workdir *ent;
	char *dir_name;
	int toplevel;

	if (dir == NULL) {
		dir_name = NULL;
		toplevel = 1;
	} else {
		dir_name = dir->name;
		toplevel = 0;
	}

	XZALLOC(ent);
	ent->fd = -1;
	ent->toplevel = toplevel;
	ent->name = xcatfile(dir_name, name);
	ent->parent = dir;
	dir_list_link_tail(&dir_list_head, ent);

	return ent;
}

int
workdir_get(struct workdir *ent)
{
	if (ent->fd == -1) {
		ent->fd = open(ent->name, O_RDONLY | O_NONBLOCK | O_DIRECTORY);
		if (ent->fd != -1) {
			if (dir_list_count(&dir_list_lru) == open_dir_max) {
				struct workdir *p = dir_list_lru.tail;
				workdir_close(p);
				dir_list_unlink(&dir_list_lru, p);
				dir_list_link_tail(&dir_list_head, p);
			}
			dir_list_unlink(&dir_list_head, ent);
			dir_list_link_head(&dir_list_lru, ent);
		}
	}
	return ent->fd;
}

static struct file_entry *filetab;
static size_t file_count;
static size_t file_max;

void
file_add(struct workdir *dir, char const *name)
{
	struct file_entry *fent;
	struct stat st;

	if (fstatat(dir->fd, name, &st, AT_SYMLINK_NOFOLLOW) != 0) {
		if (errno == ENOENT)
			return;
		error(EX_FAIL, errno, "can't stat %s/%s", dir->name, name);
	}

	if (S_ISDIR(st.st_mode)) {
		workdir_add(dir, name);
		return;
	}

	if (fm && !file_match(name, &st, fm)) {
		workdir_keep(dir);
		return;
	}

	if (file_count == file_max) {
		filetab = x2nrealloc(filetab, &file_max, sizeof(filetab[0]));
	}
	fent = &filetab[file_count++];
	fent->dir = dir;
	fent->st_dev = st.st_dev;
	fent->st_ino = st.st_ino;
	fent->name = xstrdup(name);
}

void
file_add_dir(struct workdir *dir)
{
	struct file_entry *fent;
	struct stat st;

	if (!remove_top_dir && dir->toplevel)
		return;

	if (dir->keep)
		return;

	if (fstat(dir->fd, &st) != 0) {
		if (errno != ENOENT) {
			exit_code = EX_ERRORS;
			error(0, errno, "can't stat %s", dir->name);
		}
	}

	if (file_count == file_max) {
		filetab = x2nrealloc(filetab, &file_max, sizeof(filetab[0]));
	}
	fent = &filetab[file_count++];
	fent->dir = NULL;
	fent->st_dev = st.st_dev;
	fent->st_ino = st.st_ino;
	fent->name = xstrdup(dir->name);
}

static int
file_ent_cmp(const void *a, const void *b)
{
	const struct file_entry *fa = a;
	const struct file_entry *fb = b;

	if (fa->st_dev < fb->st_dev)
		return -1;
	else if (fa->st_dev > fb->st_dev)
		return 1;

	if (fa->st_ino < fb->st_ino)
		return -1;
	else if (fa->st_ino > fb->st_ino)
		return 1;

	return 0;
}

static int
dir_name_cmp(const void *a, const void *b)
{
	const struct file_entry *fa = a;
	const struct file_entry *fb = b;
	ssize_t alen = strlen(fa->name);
	ssize_t blen = strlen(fb->name);
	return blen - alen;
}

void
file_sort(void)
{
	if (verbose)
		printf("sorting files\n");
	qsort(filetab, file_count, sizeof(filetab[0]), file_ent_cmp);
}

void
file_remove(int flag)
{
	size_t i;
	if (verbose)
		printf("removing %zu %s\n", file_count,
		       flag ? "directories" : "files");
	for (i = 0; i < file_count; i++) {
		struct file_entry *file = &filetab[i];
		int dfd;
		char *dir_name;

		checkpoint();

		if (file->dir) {
			if ((dfd = workdir_get(file->dir)) == -1) {
				if (errno == ENOENT)
					continue;
				error(EX_FAIL, errno, "can't open directory %s",
				      file->dir->name);
			}
			dir_name = file->dir->name;
		} else {
			dfd = AT_FDCWD;
			dir_name = NULL;
		}

		if (verbose > 2) {
			if (dir_name)
				printf("removing %s/%s\n", dir_name, file->name);
			else
				printf("removing %s\n", file->name);
		}
		if (!dry_run && unlinkat(dfd, file->name, flag) &&
		    errno != ENOENT) {
			if (file->dir)
				workdir_keep(file->dir);
			exit_code = EX_ERRORS;
			if (dir_name)
				error(0, errno, "removing %s/%s", dir_name, file->name);
			else
				error(0, errno, "removing %s", file->name);
		}
	}
}

void
dir_remove(void)
{
	struct workdir *dir;

	file_count = 0;
	do {
		while ((dir = dir_list_lru.head) != NULL) {
			dir_list_unlink(&dir_list_lru, dir);
			file_add_dir(dir);
			workdir_close(dir);
			// FIXME: free dir
		}
		while ((dir = dir_list_head.head) != NULL) {
			if (workdir_get(dir) != -1)
				break;
			else if (errno == ENOENT) {
				dir_list_unlink(&dir_list_head, dir);
				// FIXME: free dir
			} else
				error(EX_FAIL, errno, "can't open directory %s",
				      dir->name);
		}
	} while (dir_list_lru.head);

	qsort(filetab, file_count, sizeof(filetab[0]), dir_name_cmp);
	file_remove(AT_REMOVEDIR);
}

void
workdir_collect(struct workdir *dir)
{
	DIR *dp;
	struct dirent *ent;
	int dfd, fd;

	if (verbose > 1)
		printf("collecting directory %s\n", dir->name);

	if ((dfd = workdir_get(dir)) == -1) {
		if (errno == ENOENT)
			return;
		error(EX_FAIL, errno, "can't open directory %s", dir->name);
	}

	fd = dup(dfd);
	if (fd == -1) {
		error(EX_FAIL, errno, "dup failed");
	}

	dp = fdopendir(fd);
	if (!dp) {
		error(EX_FAIL, errno, "can't open %s", dir->name);
	}

	while ((ent = readdir(dp)) != NULL) {
		if (strcmp(ent->d_name, ".") == 0 ||
		    strcmp(ent->d_name, "..") == 0)
			continue;
		file_add(dir, ent->d_name);
	}

	closedir(dp);

	if (verbose > 1)
		printf("total files collected: %zu\n", file_count);
}

void
purgedir_collect(char **argv)
{
	struct workdir *dir;
	struct dir_list temp = DIR_LIST_INITIALIZER;
	struct stat st;
	char const *name;

	if (verbose)
		printf("collecting directories\n");

	while ((name = *argv++) != NULL) {
		if (fstatat(AT_FDCWD, name, &st, AT_SYMLINK_NOFOLLOW) != 0) {
			error(EX_FAIL, errno, "can't stat %s", name);
		}

		if (!S_ISDIR(st.st_mode)) {
			error(EX_FAIL, 0,
			      "top-level argument %s is not a directory",
			      name);
		}

		dir = workdir_add(NULL, name);
		if (dir == NULL)
			error(EX_FAIL, 0, "%s: no such directory", name);

		do {
			if (workdir_get(dir) == -1) {
				if (errno == ENOENT) {
					dir_list_unlink(&dir_list_head, dir);
					// FIXME: free dir
					continue;
				}
				error(EX_FAIL, errno, "can't open directory %s",
				      dir->name);
			}
			dir_list_unlink(&dir_list_lru, dir);
			dir_list_link_tail(&temp, dir);
			workdir_collect(dir);
			workdir_close(dir);
		} while ((dir = dir_list_head.head) != NULL);
	}

	dir_list_head = temp;
}

static void *
thr_pgd(void *arg)
{
	purgedir_collect((char**)arg);
	file_sort();
	file_remove(0);
	if (!keep_dirs)
		dir_remove();
	exit(EX_OK);
}

int fatal_signals[] = {
    SIGHUP,
    SIGINT,
    SIGQUIT,
    SIGTERM,
    0
};

static void
signull(int sig)
{
}

static void
block_signals(void)
{
	struct sigaction act;
	sigset_t sigs;
	int i;

	sigemptyset(&sigs);

	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	act.sa_handler = signull;

	for (i = 0; fatal_signals[i]; i++) {
		sigaddset(&sigs, fatal_signals[i]);
		sigaction(fatal_signals[i], &act, NULL);
	}
	sigaddset(&sigs, SIGPIPE);
	sigaddset(&sigs, SIGALRM);
	sigaddset(&sigs, SIGCHLD);
	pthread_sigmask(SIG_BLOCK, &sigs, NULL);
}

static void
wait_signals(void)
{
	sigset_t sigs;
	int i;

	/* Create a set of signals to wait for */
	sigemptyset(&sigs);
	for (i = 0; fatal_signals[i]; i++) {
		sigaddset(&sigs, fatal_signals[i]);
	}

	sigwait(&sigs, &i);
}


size_t
get_num(char const *arg)
{
	unsigned long n;
	char *p;
	errno = 0;
	n = strtoul(arg, &p, 10);
	if (errno != 0 || *p != 0)
		error(EX_USAGE, 0, "bad number: %s", arg);
	return n;
}

struct thread_info {
	pthread_t tid;              /* Thread handle. Gets filled when the
				       thread is created. */
	void *(*start)(void *ptr);  /* Thread start routine. */
	void *arg;
	/* Each of the function pointers below can be NULL. */
	int (*init)(void);          /* Initialization function.  Called at
				       startup before creating the thread. */
	void (*stop) (pthread_t);   /* Function to stop the thread */
};

static void
start_threads(struct thread_info *threads, int nthr)
{
	int i;

	for (i = 0; i < nthr; i++) {
		if (threads[i].init) {
			if (threads[i].init())
				continue;
		}
		if (threads[i].start)
			pthread_create(&threads[i].tid, NULL,
				       threads[i].start,
				       threads[i].arg);
	}
}

static void
stop_threads(struct thread_info *threads, int nthr)
{
	int i;
	for (i = nthr - 1; i >= 0; i--) {
		if (threads[i].tid != 0) {
			if (threads[i].stop)
				threads[i].stop(threads[i].tid);
		}
	}
}

enum {
	OPT_MAXDIR,
	OPT_INTERVAL,
	OPT_LA,
	OPT_DRY_RUN,
	OPT_REMOVE_DIR,
	OPT_KEEP_DIRS,
	OPT_VERBOSE,

	OPT_AMIN,
	OPT_ATIME,
	OPT_CMIN,
	OPT_CTIME,
	OPT_MMIN,
	OPT_MTIME,
	OPT_DAYSTART,
	OPT_NAME,
	OPT_NOT,
	OPT_OR,
	OPT_TYPE,
	OPT_PERM,
	OPT_UID,
	OPT_GID,

	OPT_VERSION,
	OPT_HELP,
};

static struct optdef options[] = {
	{ "", NULL, OPTFLAG_DOC, 0,
	  "Arguments can be specified before options or interspersed with them." },
	{ "", NULL, OPTFLAG_DOC, 0,
	  "General options" },
	{ "max-dir", "N", OPTFLAG_DEFAULT, OPT_MAXDIR,
	  "Limit number of simultaneously open directories to N."
	  " Default: %[open_dir_max]." },
	{ "d", NULL, OPTFLAG_ALIAS },
	{ "dry-run", NULL, OPTFLAG_DEFAULT, OPT_DRY_RUN,
	  "Dry run: do nothing, print what would have been done." },
	{ "n", NULL, OPTFLAG_ALIAS },
	{ "remove-dir", NULL, OPTFLAG_DEFAULT, OPT_REMOVE_DIR,
	  "Remove the directory itself." },
	{ "r", NULL, OPTFLAG_ALIAS },
	{ "keep", NULL, OPTFLAG_DEFAULT, OPT_KEEP_DIRS,
	  "Keep directory hierarchy; don't remove directories." },
	{ "verbose", NULL, OPTFLAG_DEFAULT, OPT_VERBOSE,
	  "Increase verbosity" },
	{ "v", NULL, OPTFLAG_ALIAS },

	{ "", NULL, OPTFLAG_DOC, 0,
	  "LA control options" },
	{ "la-interval", "T", OPTFLAG_DEFAULT, OPT_INTERVAL,
	  "Check LA each T seconds (floating point number allowed)."
	  " Default: %[checkpoint_interval]." },
	{ "i", NULL, OPTFLAG_ALIAS },
	{ "la-range", "MIN[-MAX]", OPTFLAG_DEFAULT, OPT_LA,
	  "Throttle if LA reaches MAX; unthrottle when it falls below MIN." },
	{ "l", NULL, OPTFLAG_ALIAS },

	{ "", NULL, OPTFLAG_DOC, 0, "File selection predicates" },
	{ "amin", "[+-]N", OPTFLAG_DEFAULT, OPT_AMIN,
	  "Select files by atime in minutes." },
	{ "atime", "[+-]N", OPTFLAG_DEFAULT, OPT_ATIME,
	  "Select files by atime in days." },
	{ "cmin", "[+-]N", OPTFLAG_DEFAULT, OPT_CMIN,
	  "Select files by ctime in minutes." },
	{ "ctime", "[+-]N", OPTFLAG_DEFAULT, OPT_CTIME,
	  "Select files by ctime in days." },
	{ "mmin", "[+-]N", OPTFLAG_DEFAULT, OPT_MMIN,
	  "Select files by mtime in minutes." },
	{ "mtime", "[+-]N", OPTFLAG_DEFAULT, OPT_MTIME,
	  "Select files by mtime in days." },
	{ "", NULL, OPTFLAG_DOC, 0,
	  "For the above options, if argument starts with +, files older than"
	  " the specified amount of time ago are selected.  If it starts with"
	  " -, files newer than that amount of time are selected." },
	{ "daystart", NULL, OPTFLAG_DEFAULT, OPT_DAYSTART,
	  "Measure times from the beginning of today rather than from 24 hours"
	  " ago." },
	{ "name", "PATTERN", OPTFLAG_DEFAULT, OPT_NAME,
	  "Select only files with names matching PATTERN." },
	{ "type", "b|c|p|f|l|s", OPTFLAG_DEFAULT, OPT_TYPE,
	  "Files of this type: block device, character device, FIFO,"
	  " regular file, symlink, socket." },
	{ "perm", "[-/]MODE", OPTFLAG_DEFAULT, OPT_PERM,
	  "Files with matching MODE (octal).  '-' requires that all of the"
	  " permission bits be set, '/' requires that any of them be set." },
	{ "uid", "UID", OPTFLAG_DEFAULT, OPT_UID,
	  "Files owned by this UID." },
	{ "gid", "GID", OPTFLAG_DEFAULT, OPT_GID,
	  "Files owned by this GID." },

	{ "not", NULL, OPTFLAG_DEFAULT, OPT_NOT,
	  "Negate the effect of the next predicate." },
	{ "or", NULL, OPTFLAG_DEFAULT, OPT_OR,
	  "Join two adjacent predicates by logical OR." },

	{ "", NULL, OPTFLAG_DOC, 0,
	  "Parentheses may be used to change predicate precedence. "
	  "Be sure to quote them, to prevent shell expansion." },

	{ "", NULL, OPTFLAG_DOC, 0, "Informative options" },
	{ "help", NULL, OPTFLAG_DEFAULT, OPT_HELP,
	  "Display this help list." },
	{ "version", NULL, OPTFLAG_DEFAULT, OPT_VERSION,
	  "Display program version." },
	{ "V", NULL, OPTFLAG_ALIAS },
	{ NULL }
};

static struct docmacro docmacros[] = {
	{
		.keyword = "progname",
		.type = docmacro_string,
		.data = &progname,
	},
	{
		.keyword = "open_dir_max",
		.type = docmacro_size,
		.constant = 1,
		.data = (void*)(intptr_t)QRM_DIR_MAX
	},
	{
		.keyword = "checkpoint_interval",
		.type = docmacro_timespec,
		.data = &checkpoint_interval
	},
	{ NULL }
};

static struct parseopt po = {
	.package_name = PACKAGE_NAME,
	.package_bugreport = PACKAGE_BUGREPORT,
	.descr = "Recursively removes directory contents.",
	.argdoc = "[OPTIONS] DIR...",
	.optdef = options,
	.docmacros = docmacros,
	.flags = OPTF_PERMUTE
};

static int copyright_year = 2024;

static void
version(void)
{
	printf("%s (%s) %s\n", progname, PACKAGE_NAME, PACKAGE_VERSION);
	printf("Copyright (C) %d Sergey Poznyakoff\n", copyright_year);
	printf("\
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n\
");
}

int
main(int argc, char **argv)
{
	int c;
	struct thread_info threads[] = {
		{
			.init = checkpoint_init,
			.start = thr_checkpoint
		},
		{
			.start = thr_pgd
		}
	};
	int nthr = sizeof(threads)/sizeof(threads[0]);
	char *arg;
	int daystart = 0;

	parseopt_init (&po, argc, argv);
	while ((c = parseopt_next (&po, &arg)) != EOF) {
		switch (c) {
		case OPT_MAXDIR:
			open_dir_max = get_num(arg);
			if (open_dir_max == 0)
				error(EX_USAGE, 0,
				      "argument to -d must be greater than 0");
			break;

		case OPT_INTERVAL:
			set_checkpoint_interval(arg);
			break;

		case OPT_HELP:
			usage (EX_OK, &po);

		case OPT_LA:
			set_max_la(arg);
			break;

		case OPT_DRY_RUN:
			dry_run++;
			break;

		case OPT_REMOVE_DIR:
			remove_top_dir = 1;
			break;

		case OPT_KEEP_DIRS:
			keep_dirs = 1;
			break;

		case OPT_VERBOSE:
			verbose++;
			break;

		case OPT_VERSION:
			version();
			exit(EX_OK);

		/* File selection */
		case OPT_AMIN:
			fm = filematch_cond_time(fm, arg, TIME_ATIME,
						 MINUTES, daystart);
			break;

		case OPT_ATIME:
			fm = filematch_cond_time(fm, arg, TIME_ATIME,
						 DAYS, daystart);
			break;

		case OPT_CMIN:
			fm = filematch_cond_time(fm, arg, TIME_CTIME,
						 MINUTES, daystart);
			break;

		case OPT_CTIME:
			fm = filematch_cond_time(fm, arg, TIME_CTIME,
						 DAYS, daystart);
			break;

		case OPT_MMIN:
			fm = filematch_cond_time(fm, arg, TIME_MTIME,
						 MINUTES, daystart);
			break;

		case OPT_MTIME:
			fm = filematch_cond_time(fm, arg, TIME_MTIME,
						 DAYS, daystart);
			break;

		case OPT_DAYSTART:
			daystart = 1;
			break;

		case OPT_NAME:
			fm = filematch_cond_glob(fm, arg);
			break;

		case OPT_TYPE:
			fm = filematch_cond_type(fm, arg);
			break;

		case OPT_PERM:
			fm = filematch_cond_perm(fm, arg);
			break;

		case OPT_UID:
			fm = filematch_cond_uid(fm, arg);
			break;

		case OPT_GID:
			fm = filematch_cond_gid(fm, arg);
			break;

		case OPT_NOT:
			fm = filematch_cond_push_not(fm);
			break;

		case OPT_OR:
			fm = filematch_cond_push_or(fm);
			break;

		case '(':
			fm = filematch_cond_push(fm);
			break;

		case ')':
			fm = filematch_cond_pop(fm);
			break;

		default:
			error(1, 0, "try \"%s -h\" for help", progname);
		}
	}

	filematch_finish(fm);

	if (keep_dirs && remove_top_dir)
		error(EX_USAGE, 0, "conflicting options: -remove-dir -keep");

	if (dry_run)
		verbose++;

	argc = parseopt_argc(&po);
	if (argc < 1)
		error(EX_USAGE, 0,
		      "not enough arguments; try \"%s -h\" for help",
		      progname);

	threads[1].arg = parseopt_argv(&po);

	block_signals();
	start_threads(threads, nthr);
	wait_signals();
	stop_threads(threads, nthr);
	return exit_code;
}
