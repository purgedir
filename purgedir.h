/* This file is part of purgedir.
   Copyright (C) 2024 Sergey Poznyakoff

   Purgedir is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Purgedir is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with purgedir.  If not, see <http://www.gnu.org/licenses/>. */

#include "mem.h"

#ifndef QRM_DIR_MAX
# define QRM_DIR_MAX 16
#endif
#define NANOSEC 1000000000L

extern int verbose;
extern struct timespec checkpoint_interval;

void set_max_la(char *arg);
void set_checkpoint_interval(char *arg);

int checkpoint_init(void);
extern void (*checkpoint)(void);

void *thr_checkpoint(void *arg);

enum {
	MINUTES = 60,
	DAYS = 86400
};

typedef struct filecond_list FILECOND_LIST;

struct stat;
int file_match(char const *name, struct stat *st, struct filecond_list const *list);

enum {
	TIME_ATIME,
	TIME_CTIME,
	TIME_MTIME
};

struct filecond_list *filematch_cond_time(struct filecond_list *list,
					  char const *arg, int kind,
					  int mult, int daystart);
struct filecond_list *filematch_cond_glob(struct filecond_list *list,
					  char const *arg);
struct filecond_list *filematch_cond_push_not(struct filecond_list *list);
struct filecond_list *filematch_cond_push_or(struct filecond_list *list);
struct filecond_list *filematch_cond_push(struct filecond_list *list);
struct filecond_list *filematch_cond_pop(struct filecond_list *list);
struct filecond_list *filematch_cond_type(struct filecond_list *list, char const *arg);
struct filecond_list *filematch_cond_perm(struct filecond_list *list, char const *arg);
struct filecond_list *filematch_cond_uid(struct filecond_list *list, char const *arg);
struct filecond_list *filematch_cond_gid(struct filecond_list *list, char const *arg);

void filematch_finish(struct filecond_list *);

size_t get_num(char const *arg);
