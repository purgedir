/* This file is part of purgedir.
   Copyright (C) 2023-2024 Sergey Poznyakoff

   Purgedir is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Purgedir is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with purgedir.  If not, see <http://www.gnu.org/licenses/>. */

#define OPTFLAG_DEFAULT 0
#define OPTFLAG_ALIAS   0x1
#define OPTFLAG_DOC     0x2

struct optdef {
	char *opt_name;
	char *opt_argdoc;
	int opt_flags;
	int opt_code;
	void *opt_doc;
};

enum docmacrotype {
	docmacro_string,
	docmacro_timespec,
	docmacro_size
};

struct docmacro {
	char const *keyword;
	int constant;
	int type;
	void *data;
};

#define OPTF_DEFAULT 0
#define OPTF_PERMUTE 0x1

struct parseopt {
	char *descr;
	char *argdoc;
	struct optdef *optdef;
	int *optidx;
	int optcount;
	struct docmacro *docmacros;
	int flags;

	char *package_name;
	char *package_bugreport;
	char *package_url;
	
	int argc;
	char **argv;
	int argi;
	int argstart;
};

static inline int
parseopt_argc(struct parseopt *po)
{
	return po->argc - po->argi;
}

static inline char **
parseopt_argv(struct parseopt *po)
{
	return po->argv + po->argi;
}


int parseopt_next(struct parseopt *po, char **ret_arg);
int parseopt_init(struct parseopt *po, int argc, char **argv);
void usage(int err, struct parseopt *po);

extern const char *progname;

enum {
	EX_OK,
	EX_FAIL,
	EX_ERRORS,
	EX_USAGE
};

void error(int ex, int err, char const *fmt, ...);
