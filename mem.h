void *xmalloc(size_t size);
void *xcalloc(size_t nmemb, size_t size);
char *xstrdup(char const *str);
void *xrealloc(void *ptr, size_t size);
void *x2nrealloc(void *p, size_t *pn, size_t s);
char *xcatfile(char const *dir, char const *name);

#define XZALLOC(p) (p = xcalloc(1, sizeof(*p)))

