#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include <limits.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "../parseopt.h"
#include "../mem.h"

struct namebuf {
	char *base;
	size_t dirlen;
	size_t maxlen;
};

struct namebuf *
namebuf_init(size_t maxlen)
{
	struct namebuf *nb = malloc(sizeof(*nb));
	assert(nb != NULL);
	nb->base = malloc(maxlen);
	assert(nb->base != NULL);
	nb->dirlen = 0;
	nb->maxlen = maxlen;
	return nb;
}

static char *
namebuf_name(struct namebuf *nb, char const *fmt, ...)
{
	va_list ap;
	size_t n, bufsize = nb->maxlen - nb->dirlen;
	va_start(ap, fmt);
	n = vsnprintf(nb->base + nb->dirlen, bufsize, fmt, ap);
	va_end(ap);
	if (n < 0 || n >= bufsize ||
	    !memchr (nb->base + nb->dirlen, '\0', n + 1))
		error(1, 0, "file name too long");
	return nb->base;
}

static size_t
namebuf_dir(struct namebuf *nb)
{
	size_t n = nb->dirlen;
	nb->dirlen = strlen(nb->base);
	if (nb->dirlen == nb->maxlen - 1)
		error(1, 0, "file name too long");
	nb->base[nb->dirlen++] = '/';
	return n;
}

static void
namebuf_updir(struct namebuf *nb, size_t n)
{
	nb->dirlen = n;
}

struct hierdef {
	int level;    /* Directory nesting level. */
	int ndirs;    /* Number of subdirectories in each directory. */
	int nfiles;   /* Number of files in leaf directories. */
	int non_leaf; /* Create files in non-leaf directories as well. */
	size_t file_size_min;  /* Minimum file size. */
	size_t file_size_max;  /* Maximum file size. */
	struct timespec *times; /* File mtime and atime. */
};

static void
ffill(FILE *fp, size_t length)
{
	size_t i;

	for (i = 0; i < length; i++)
		fputc(i & 255, fp);
}

void
genfile(char const *name, struct hierdef const *hdef)
{
	FILE *fp;
	size_t size;

	if (hdef->file_size_min == hdef->file_size_max)
		size = hdef->file_size_min;
	else
		size = hdef->file_size_min +
			random() % (hdef->file_size_max - hdef->file_size_min);

	fp = fopen(name, "w");
	if (fp == NULL) {
		error(1, errno, "%s", name);
	}

	ffill(fp, size);
	fclose(fp);

	if (hdef->times)
		utimensat(AT_FDCWD, name, hdef->times, 0);
}

static int
numwidth(int num)
{
	unsigned u;
	unsigned nw = 0;

	assert(num >= 0);
	u = num;
	do {
		nw++;
	} while ((u >>= 4) != 0);

	assert(nw <= INT_MAX);
	
	return nw;
}	

void
createdir(struct namebuf *nb, struct hierdef const *hdef, int level)
{
	int i, w, dirlen;
	
	if (mkdir(nb->base, 0755))
		error(1, errno, "can't create directory %s", nb->base);

	dirlen = namebuf_dir(nb);

	if (hdef->nfiles && (hdef->level == level || hdef->non_leaf)) {
		w = numwidth(hdef->nfiles);
		for (i = 0; i < hdef->nfiles; i++) {
			genfile(namebuf_name(nb, "f%0*x", w, i), hdef); 
		}
	}
	if (hdef->ndirs && level < hdef->level) {
		w = numwidth(hdef->ndirs);
		for (i = 0; i < hdef->ndirs; i++) {
			namebuf_name(nb, "d%0*x", w, i);
			createdir(nb, hdef, level + 1);
		}
	}
	
	namebuf_updir(nb, dirlen);
}

enum {
	OPT_FILES = 256,
	OPT_NON_LEAF,
	OPT_SIZE,
	OPT_ATIME,
	OPT_AMIN,
	OPT_MTIME,
	OPT_MMIN,
	OPT_DAYSTART,
	OPT_DIRS,
	OPT_LEVEL,
	OPT_HELP
};

static struct optdef options[] = {
	{ "", NULL, OPTFLAG_DOC, 0,
	  "File creation options:" },
	{ "files", "N", OPTFLAG_DEFAULT, OPT_FILES,
	  "Create N files in leaf directories." },
	{ "non-leaf", NULL, OPTFLAG_DEFAULT, OPT_NON_LEAF,
	  "Create files in non-leaf directories as well." },
	{ "size", "N[-N]", OPTFLAG_DEFAULT, OPT_SIZE,
	  "Size of each file."
	  " If range is supplied, the actual value to be used will be"
	  " selected randomly between the supplied"
	  " minumum and maximum (inclusive)." },
	{ "atime", "[+-]DAYS",
	  OPTFLAG_DEFAULT, OPT_ATIME,
	  "Offset the atime of the created files the given number of days." },
	{ "amin", "[+-]MINS",
	  OPTFLAG_DEFAULT, OPT_AMIN,
	  "Offset the mtime of the created files the given number of minutes." },
	{ "mtime", "[+-]DAYS",
	  OPTFLAG_DEFAULT, OPT_MTIME,
	  "Offset the mtime of the created files the given number of days." },
	{ "mmin", "[+-]MINS",
	  OPTFLAG_DEFAULT, OPT_MMIN,
	  "Offset the mtime of the created files the given number of minutes." },
	{ "daystart", NULL, OPTFLAG_DEFAULT, OPT_DAYSTART,
	  "Measure times from the beginning of today rather than from 24 hours"
	  " ago." },

	{ "", NULL, OPTFLAG_DOC, 0,
	  "Directory creation options:" },
	{ "dirs", "N", OPTFLAG_DEFAULT, OPT_DIRS,
	  "Create N directories in each directory." },
	{ "level", "N", OPTFLAG_DEFAULT, OPT_LEVEL,
	  "Set directory nesting level." },

	{ "", NULL, OPTFLAG_DOC, 0,
	  "Informational options:" },
	{ "help", NULL, OPTFLAG_DEFAULT, OPT_HELP,
	  "Show this help text." },
	{ NULL }
};

static struct parseopt po = {
	.descr = "Create test directory hierarchy.",
	.argdoc = "[OPTIONS] DIR",
	.optdef = options,
};

size_t
get_num(char const *arg, char **endp)
{
	unsigned long n;
	char *p;
	errno = 0;
	n = strtoul(arg, &p, 10);
	if (errno != 0 || (*p != 0 && endp == NULL))
		error(EX_USAGE, 0, "bad number: %s", arg);
	if (endp)
		*endp = p;
	return n;
}

void
get_range(char const *arg, size_t *pmin, size_t *pmax)
{
	char *p;
	*pmin = get_num(arg, &p);
	if (*p == 0)
		*pmax = *pmin;
	else if (*p == '-') {
		*pmax = get_num(p + 1, NULL);
		if (*pmin > *pmax) {
			size_t t = *pmax;
			*pmax = *pmin;
			*pmin = t;
		}
	} else
		error(EX_USAGE, 0, "bad number or range: %s (near %s)",
		      arg, p);
}

static int daystart_option;
static struct timespec *now;
enum { MINSECS = 60, DAYSECS = 86400 };

void
get_times(char *arg, int i, unsigned factor, struct timespec **times)
{
	char *p;
	struct timespec *ts;
	long n;
	
	if (!now) {
		now = xcalloc(1, sizeof(*now));
		clock_gettime(CLOCK_REALTIME, now);
	}

	if (*times == NULL) {
		ts = xcalloc(2, sizeof(*ts));
		ts[0] = ts[1] = *now;
		*times = ts;
	} else
		ts = *times;

	/* FIXME: Use set_checkpoint_interval and timespec_add from
	   ckp.c */
	errno = 0;
	n = strtol(arg, &p, 10);
	if (errno || *p)
		error(EX_USAGE, 0, "bad time offset: %s", arg);

	ts[i].tv_sec = now->tv_sec + n * factor;
	if (daystart_option)
		ts[i].tv_sec -= ts[i].tv_sec % DAYSECS;
	else {
		ts[i].tv_sec -= DAYSECS;
		ts[i].tv_nsec = 0;
	}
}

int
main(int argc, char **argv)
{
	char *arg;
	struct namebuf *nb;
	struct hierdef hdef;
	int c;

	memset(&hdef, 0, sizeof(hdef));
	srandom((unsigned)time(NULL));
	
	parseopt_init(&po, argc, argv);
	while ((c = parseopt_next(&po, &arg)) != EOF) {
		switch (c) {
		case OPT_FILES:
			hdef.nfiles = get_num(arg, NULL);
			break;

		case OPT_NON_LEAF:
			hdef.non_leaf = 1;
			break;
			
		case OPT_SIZE:
			get_range(arg, &hdef.file_size_min, &hdef.file_size_max);
			break;

		case OPT_AMIN:
			get_times(arg, 0, MINSECS, &hdef.times);
			break;
			
		case OPT_ATIME:
			get_times(arg, 0, DAYSECS, &hdef.times);
			break;
			
		case OPT_MMIN:
			get_times(arg, 1, MINSECS, &hdef.times);
			break;
			
		case OPT_MTIME:
			get_times(arg, 1, DAYSECS, &hdef.times);
			break;

		case OPT_DAYSTART:
			daystart_option = 1;
			break;
			
		case OPT_DIRS:
			hdef.ndirs = get_num(arg, NULL);
			break;
			
		case OPT_LEVEL:
			hdef.level = get_num(arg, NULL);
			break;
			
		case OPT_HELP:
			usage(EX_OK, &po);
			
		default:
			error(1, 0, "try \"%s -h\" for help", progname);
		}
	}

	if (parseopt_argc(&po) != 1)
		error(EX_USAGE, 0,
		      "bad number of arguments; try \"%s -h\" for help",
		      progname);
	argv = parseopt_argv(&po);

	nb = namebuf_init(1024); // FIXME
	namebuf_name(nb, "%s", argv[0]);
	createdir(nb, &hdef, 0);
	return 0;
}

