/* This file is part of purgedir.
   Copyright (C) 2024 Sergey Poznyakoff

   Purgedir is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Purgedir is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with purgedir.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stddef.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fnmatch.h>
#include <errno.h>
#include "purgedir.h"
#include "parseopt.h"

enum filecond_type {
	COND_NONE,
	COND_SEQ,
	COND_NOT,
	COND_OR,
	COND_TIME,
	COND_GLOB,
	COND_TYPE,
	COND_PERM_EXACT,
	COND_PERM_ALL,
	COND_PERM_ANY,
	COND_UID,
	COND_GID
};

struct filecond_time {
	size_t offset;
	time_t value;
	int gt;
};

struct filecond {
	struct filecond *next, *prev;
	enum filecond_type type;
	union {
		struct filecond *cond;
		struct filecond *cond_or[2];
		struct filecond_list *cond_seq;
		struct filecond_time cond_time;
		char *cond_glob;
		int cond_mode;
		unsigned long cond_id;
	};
};

struct filecond_list {
	struct filecond *head, *tail;
	int type;
	struct filecond_list *up;
};

static struct filecond_list *
_filecond_list_append(struct filecond_list *list, struct filecond *cond)
{
	if (!list)
		XZALLOC(list);
	cond->prev = list->tail;
	if (list->tail)
		list->tail->next = cond;
	else
		list->head = cond;
	list->tail = cond;
	return list;
}

static struct filecond_list *
_filecond_list_append_list(struct filecond_list *a, struct filecond_list *b)
{
	if (b) {
		if (!a)
			XZALLOC(a);
		if (b->head)
			b->head->prev = a->tail;

		if (a->tail)
			a->tail->next = b->head;
		else
			a->head = b->head;
		a->tail = b->tail;

		b->head = b->tail = NULL;
	}
	return a;
}

static struct filecond *
_filecond_list_remove_tail(struct filecond_list *list)
{
	struct filecond *cond = list->tail;
	if (cond) {
		list->tail = cond->prev;
		if (list->tail == NULL)
			list->head = NULL;
	}
	return cond;
}

struct filecond_list *
filecond_list_append(struct filecond_list *list, struct filecond *cond)
{
	struct filecond_list *ret;
	struct filecond *ncond;

	if (list == NULL)
		XZALLOC(list);
	switch (list->type) {
	case COND_OR:
		ret = list->up;
		XZALLOC(ncond);
		ncond->type = COND_OR;
		ncond->cond_or[0] = _filecond_list_remove_tail(ret);
		ncond->cond_or[1] = cond;
		free(list);
		return filecond_list_append(ret, ncond);

	case COND_NOT:
		list = _filecond_list_append(list, cond);
		ret = list->up;
		XZALLOC(ncond);
		ncond->type = COND_NOT;
		ncond->cond = cond;
		free(list);
		return filecond_list_append(ret, ncond);

	case COND_SEQ:
		if (cond->type == COND_SEQ) {
			list = _filecond_list_append_list(list, cond->cond_seq);
			free(cond->cond_seq);
			break;
		}
		/* fall through */

	default:
		list = _filecond_list_append(list, cond);
	}
	return list;
}

static inline int
filecond_time_match(struct stat *st, struct filecond_time const *cond)
{
	time_t *t = (time_t*)((char*)st + cond->offset);
	int result = *t < cond->value;
	if (cond->gt)
		result = !result;
	return result;
}

static int
filecond_match(char const *name, struct stat *st, struct filecond const *cond)
{
	for (; cond; cond = cond->next) {
		switch (cond->type) {
		case COND_SEQ:
			if (!filecond_match(name, st, cond->cond_seq->head))
				return 0;
			break;

		case COND_NOT:
			if (filecond_match(name, st, cond->cond))
				return 0;
			break;

		case COND_OR:
			if (!filecond_match(name, st, cond->cond_or[0]) &&
			    !filecond_match(name, st, cond->cond_or[1]))
				return 0;
			break;

		case COND_TIME:
			if (!filecond_time_match(st, &cond->cond_time))
				return 0;
			break;

		case COND_GLOB:
			if (fnmatch(cond->cond_glob, name, FNM_PATHNAME))
				return 0;
			break;

		case COND_TYPE:
			if ((st->st_mode & S_IFMT) != cond->cond_mode)
				return 0;
			break;

		case COND_PERM_EXACT:
			if ((st->st_mode & 0777) != cond->cond_mode)
				return 0;
			break;

		case COND_PERM_ANY:
			if (!(st->st_mode & cond->cond_mode))
				return 0;
			break;

		case COND_PERM_ALL:
			if ((st->st_mode & cond->cond_mode) != cond->cond_mode)
				return 0;
			break;

		case COND_UID:
			if (st->st_uid != cond->cond_id)
				return 0;
			break;

		case COND_GID:
			if (st->st_gid != cond->cond_id)
				return 0;
			break;

		default:
			abort();
		}
	}
	return 1;
}

int
file_match(char const *name, struct stat *st, struct filecond_list const *list)
{
	return filecond_match(name, st, list->head);
}

struct filecond_list *
filematch_cond_time(struct filecond_list *list, char const *arg, int kind,
		    int mult, int daystart)
{
	size_t offset;
	struct filecond *cond;
	time_t t;

	switch (kind) {
	case TIME_ATIME:
		offset = offsetof(struct stat, st_atime);
		break;
	case TIME_CTIME:
		offset = offsetof(struct stat, st_ctime);
		break;
	case TIME_MTIME:
		offset = offsetof(struct stat, st_mtime);
		break;
	default:
		abort();
	}

	XZALLOC(cond);
	cond->type = COND_TIME;
	cond->cond_time.offset = offset;

	if (*arg == '+') {
		cond->cond_time.gt = 0;
		arg++;
	} else if (*arg == '-') {
		cond->cond_time.gt = 1;
		arg++;
	}

	t = time(NULL);
	if (daystart)
		t -= t % DAYS;
	else
		t -= DAYS;

	cond->cond_time.value = t - get_num(arg) * mult;
	return filecond_list_append(list, cond);
}

struct filecond_list *
filematch_cond_glob(struct filecond_list *list, char const *arg)
{
	struct filecond *cond;

	XZALLOC(cond);
	cond->type = COND_GLOB;
	cond->cond_glob = xstrdup(arg);
	return filecond_list_append(list, cond);
}

struct filecond_list *
filematch_cond_push_not(struct filecond_list *list)
{
	struct filecond_list *ret;

	XZALLOC(ret);
	ret->type = COND_NOT;
	ret->up = list;
	return ret;
}

struct filecond_list *
filematch_cond_push_or(struct filecond_list *list)
{
	struct filecond_list *ret;

	if (!list)
		error(EX_USAGE, 0, "misplaced -or");
	XZALLOC(ret);
	ret->type = COND_OR;
	ret->up = list;
	return ret;
}

struct filecond_list *
filematch_cond_push(struct filecond_list *list)
{
	struct filecond_list *ret;
	XZALLOC(ret);
	ret->type = COND_SEQ;
	ret->up = list;
	return ret;
}

struct filecond_list *
filematch_cond_pop(struct filecond_list *list)
{
	struct filecond *cond;
	struct filecond_list *ret;

	if (list == NULL || list->type != COND_SEQ)
		error(EX_USAGE, 0, "unbalanced )");
	ret = list->up;
	XZALLOC(cond);
	cond->type = COND_SEQ;
	cond->cond_seq = list;
	return filecond_list_append(ret, cond);
}

struct filecond_list *
filematch_cond_type(struct filecond_list *list, char const *arg)
{
	struct filecond *cond;

	if (arg[1])
		error(EX_USAGE, 0, "unrecognized file type: %s", arg);

	XZALLOC(cond);
	cond->type = COND_TYPE;
	switch (arg[0]) {
	case 'b':
		cond->cond_mode = S_IFBLK;
		break;

	case 'c':
		cond->cond_mode = S_IFCHR;
		break;

	case 'd':
		error(EX_USAGE, 0, "the 'd' file type is not supported");
		//cond->cond_mode = S_IFDIR;
		break;

	case 'p':
		cond->cond_mode = S_IFIFO;
		break;

	case 'f':
		cond->cond_mode = S_IFREG;
		break;

	case 'l':
		cond->cond_mode = S_IFLNK;
		break;

	case 's':
		cond->cond_mode = S_IFSOCK;
		break;

	default:
		error(EX_USAGE, 0, "unrecognized file type: %s", arg);
	}
	return filecond_list_append(list, cond);
}

struct filecond_list *
filematch_cond_perm(struct filecond_list *list, char const *arg)
{
	struct filecond *cond;
	unsigned long n;
	int type;
	char *p;

	switch (arg[0]) {
	case '-':
		type = COND_PERM_ALL;
		arg++;
		break;

	case '/':
		type = COND_PERM_ANY;
		arg++;
		break;

	default:
		type = COND_PERM_EXACT;
		break;
	}

	errno = 0;
	n = strtoul(arg, &p, 8);
	if (errno != 0 || *p)
		error(EX_USAGE, 0, "bad octal mode: %s", arg);

	if (n == 0 && type != COND_PERM_EXACT) {
		/* Both -perm -000 and -perm /000 match any file. */
		return list;
	}

	XZALLOC(cond);
	cond->type = type;
	cond->cond_mode = n;
	return filecond_list_append(list, cond);
}

static struct filecond_list *
filematch_cond_id(struct filecond_list *list, int type, char const *arg)
{
	struct filecond *cond;
	char *p;

	XZALLOC(cond);
	cond->type = type;

	errno = 0;
	cond->cond_id = strtoul(arg, &p, 10);
	if (errno != 0 || *p)
		error(EX_USAGE, 0, "bad number: %s", arg);
	return filecond_list_append(list, cond);
}

struct filecond_list *
filematch_cond_uid(struct filecond_list *list, char const *arg)
{
	return filematch_cond_id(list, COND_UID, arg);
}

struct filecond_list *
filematch_cond_gid(struct filecond_list *list, char const *arg)
{
	return filematch_cond_id(list, COND_GID, arg);
}

void
filematch_finish(struct filecond_list *list)
{
	if (list && list->up) {
		char *msg;
		switch (list->type) {
		case COND_SEQ:
			msg = "unbalanced (";
			break;

		case COND_OR:
			msg = "-or without operand";
			break;

		case COND_NOT:
			msg = "-not without operand";
			break;

		default:
			msg = "condition syntax error";
		}
		error(EX_USAGE, 0, "%s", msg);
	}
}
