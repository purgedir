/* This file is part of purgedir.
   Copyright (C) 2024 Sergey Poznyakoff

   Purgedir is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Purgedir is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with purgedir.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <math.h>
#include <pthread.h>
#include "purgedir.h"
#include "parseopt.h"

static int max_la_set;
static double min_la, max_la;
struct timespec checkpoint_interval = {
	.tv_sec = 5
};

void
set_max_la(char *arg)
{
	char *p;
	errno = 0;
	min_la = strtod(arg, &p);
	if (errno)
		error(2, 0, "bad floating point number: %s", arg);
	if (*p == 0) {
		max_la = min_la;
	} else if (*p == '-') {
		arg = p + 1;
		max_la = strtod(arg, &p);
		if (errno || *p)
			error(2, 0, "bad floating point number: %s", arg);
	} else {
		error(2, 0, "bad floating point number: %s", arg);
	}
	max_la_set = 1;
}

void
set_checkpoint_interval(char *arg)
{
	char *p;
	struct timespec ts = { 0, 0 };
	unsigned long n;

	errno = 0;
	n = strtoul(arg, &p, 10);
	if (errno)
		error(2, 0, "bad interval: %s", arg);
	ts.tv_sec = n;
	if (*p) {
		if (*p == '.') {
			int i;

			n = 0;
			p++;
			for (i = 0; i < 9 && *p; i++, p++) {
				int c = *arg - '0';
				if (c >= 0 && c < 10) {
					n = n * 10 + c;
				} else {
					error(2, 0, "bad interval: %s", arg);
				}
			}
			for (; i < 9; i++)
				n *= 10;
			ts.tv_nsec = n;
		}
	}
	checkpoint_interval = ts;
}

static inline struct timespec
timespec_add(struct timespec const *a, struct timespec const *b)
{
	struct timespec sum;

	sum.tv_sec = a->tv_sec + b->tv_sec;
	sum.tv_nsec = a->tv_nsec + b->tv_nsec;
	if (sum.tv_nsec >= NANOSEC) {
		++sum.tv_sec;
		sum.tv_nsec -= NANOSEC;
	}

	return sum;
}

static pthread_mutex_t pgd_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t pgd_cond = PTHREAD_COND_INITIALIZER;
static unsigned long pgd_count = 0;
static unsigned long max_count;
enum state {
	STATE_RUNNING,
	STATE_MAX_COUNT,
	STATE_THROTTLE
};
static enum state state = STATE_RUNNING;

static void
checkpoint_worker(void)
{
	pthread_mutex_lock(&pgd_mutex);
	if (max_count != 0 && pgd_count == max_count) {
		if (verbose)
			error(0, 0, "max_count=%lu reached; sleeping", max_count);
		for (state = STATE_MAX_COUNT; state != STATE_RUNNING; ) {
			pthread_cond_wait(&pgd_cond, &pgd_mutex);
		}
	}
	pgd_count++;
	pthread_mutex_unlock(&pgd_mutex);
}

static void
checkpoint_dummy(void)
{
}

void (*checkpoint)(void) = checkpoint_dummy;

static unsigned long
compute_unlink_count(double start_la, double end_la)
{
	double d;
	double ref_la;

	if (pgd_count == 0 || end_la < start_la)
		return 0;

	/* LA increase per removal */
	d = (end_la - start_la) / pgd_count;

	/* Reference load average */
	if (max_la > end_la)
		ref_la = end_la;
	else
		ref_la = min_la;

	/* Adjusted number of removals to keep within the allowed LA range. */
	return ceil((max_la - ref_la) / d);
}

void *
thr_checkpoint(void *arg)
{
	struct timespec ts;
	double start_la;
	double cur_la;
	unsigned long n;

	if (getloadavg(&start_la, 1) == -1) {
		error(EX_FAIL, errno, "can't get load average");
	}

	for (;;) {
		clock_gettime(CLOCK_MONOTONIC, &ts);
		ts = timespec_add(&ts, &checkpoint_interval);
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts, NULL);
		if (getloadavg(&cur_la, 1) == -1) {
			error(0, errno, "can't get load average");
			continue; // FIXME
		}

		switch (state) {
		case STATE_RUNNING:
			pthread_mutex_lock(&pgd_mutex);
			if ((n = compute_unlink_count(start_la, cur_la)) != 0)
				max_count = n;
			if (cur_la >= max_la) {
				if (verbose)
					error(0, 0, "LA %.2f: throttling; max_count=%lu", cur_la, max_count);
				state = STATE_THROTTLE;
			} else {
				pthread_mutex_unlock(&pgd_mutex);
			}
			break;

		case STATE_MAX_COUNT:
			pthread_mutex_lock(&pgd_mutex);
			if ((n = compute_unlink_count(start_la, cur_la)) != 0)
				max_count = n;
			pgd_count = 0;
			state = STATE_RUNNING;
			if (verbose)
				error(0, 0, "wake up; max_count=%lu", max_count);
			pthread_cond_broadcast(&pgd_cond);
			pthread_mutex_unlock(&pgd_mutex);
			break;

		case STATE_THROTTLE:
			if (cur_la < min_la) {
				if (verbose)
					error(0, 0, "LA %.2f: unthrottling", cur_la);
				/* max_count computed when throttling */
				pgd_count = 0;
				state = STATE_RUNNING;
				pthread_mutex_unlock(&pgd_mutex);
			}
		}

		start_la = cur_la;
	}
	return NULL;
}

int
checkpoint_init(void)
{
	if (max_la_set) {
		checkpoint = checkpoint_worker;
		return 0;
	}
	return 1;
}
