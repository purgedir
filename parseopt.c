/* This file is part of purgedir.
   Copyright (C) 2023-2024 Sergey Poznyakoff

   Purgedir is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Purgedir is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with purgedir.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <sys/ioctl.h>
#include <limits.h>
#include "parseopt.h"
#include "mem.h"

const char *progname;

static struct optdef *
option_find(struct parseopt *po, char const *optstr)
{
	size_t optlen = strlen(optstr);
	int i;
	int found = 0;
	struct optdef *found_opt;

	for (i = 0; i < po->optcount; i++) {
		struct optdef *opt = &po->optdef[po->optidx[i]];
		size_t namelen = strlen(opt->opt_name);

		if (optlen <= namelen) {
			if (memcmp(opt->opt_name, optstr, optlen) == 0) {
				switch (found) {
				case 0:
					found_opt = opt;
					found++;
					if (optlen == namelen) {
						/* Exact match */
						goto end;
					}
					break;

				case 1:
					found++;
					error(0,0,
					      "option '-%*.*s' is ambiguous; possibilities:",
					       optlen, optlen, optstr);
					fprintf (stderr, "-%s\n",
						 found_opt->opt_name);
					/* fall through */
				case 2:
					fprintf(stderr, "-%s\n", opt->opt_name);
				}
			} else if (found)
				break;
		}
	}
end:
	switch (found) {
	case 0:
		error(0, 0, "unrecognized option '-%*.*s'",
		      optlen, optlen, optstr);
		break;

	case 1:
		while (found_opt > po->optdef &&
		       (found_opt->opt_flags & OPTFLAG_ALIAS))
			found_opt--;
		return found_opt;

	case 2:
		break;
	}
	return NULL;
}

static struct docmacro *
docmacro_locate(struct docmacro *exp, char const *kw, int kwlen)
{
	for (; exp; exp++) {
		if (strlen(exp->keyword) == kwlen &&
		    memcmp(exp->keyword, kw, kwlen) == 0)
			return exp;
	}
	return NULL;
}

struct textbuf {
	char *base;
	size_t len;
	size_t cap;
};

#define TEXTBUF_INITIALIZER { NULL, 0, 0 }

static inline char *
textbuf_string(struct textbuf *buf)
{
	return buf->base;
}

static inline void
textbuf_reset(struct textbuf *buf)
{
	buf->len = 0;
}

static void
textbuf_free(struct textbuf *buf)
{
	free(buf->base);
	buf->base = NULL;
	buf->len = 0;
	buf->cap = 0;
}

static void
textbuf_append(struct textbuf *buf, char const *str, size_t len)
{
	while (buf->len + len > buf->cap)
		buf->base = x2nrealloc(buf->base, &buf->cap, 1);
	memcpy(buf->base + buf->len, str, len);
	buf->len += len;
}

static void
textbuf_append_string(struct textbuf *buf, char const *str)
{
	textbuf_append(buf, str, strlen(str));
}

static inline void
textbuf_append_char(struct textbuf *buf, char c)
{
	textbuf_append(buf, &c, 1);
}

static void
textbuf_append_ulong(struct textbuf *buf, unsigned long n, int width, int prec)
{
	size_t start = buf->len, i, j;
	do {
		static char dig[] = "0123456789";
		unsigned d = n % 10;
		textbuf_append_char(buf, dig[d]);
		n /= 10;
	} while (n > 0);
	if (width > 0) {
		for (j = buf->len - start; j < width; j++)
			textbuf_append_char(buf, '0');
	}
	for (i = start, j = buf->len-1; i < j; i++, j--) {
		int t = buf->base[i];
		buf->base[i] = buf->base[j];
		buf->base[j] = t;
	}
	if (prec > 0 && buf->len - start > prec) {
		buf->len = start + prec;
	}
}

static void
textbuf_append_timespec(struct textbuf *buf, struct timespec const *ts)
{
	textbuf_append_ulong(buf, ts->tv_sec, 0, 0);
	textbuf_append_char(buf, '.');
	textbuf_append_ulong(buf, ts->tv_nsec, 9, 3);
}

static int
docmacro_expand(struct docmacro *exp, char const *kw, int kwlen, struct textbuf *buf)
{
	struct docmacro *p = docmacro_locate(exp, kw, kwlen);
	if (!p)
		return -1;
	if (p->constant) {
		switch (p->type) {
		case docmacro_string:
			textbuf_append_string(buf, p->data);
			break;

		case docmacro_size:
			textbuf_append_ulong(buf, (size_t)(intptr_t)p->data,
					     0, 0);
			break;

		default:
			return -1;
		}
	} else {
		switch (p->type) {
		case docmacro_string:
			textbuf_append_string(buf, *(char**)p->data);
			break;

		case docmacro_timespec:
			textbuf_append_timespec(buf, p->data);
			break;

		case docmacro_size:
			textbuf_append_ulong(buf, *(size_t*)p->data,
					     0, 0);
			break;

		default:
			return -1;
		}
	}
	return 0;
}

static void
expand_vars(char const *str, struct docmacro *exp, struct textbuf *buf)
{
       while (*str) {
	       size_t len = strcspn(str, "%");
	       textbuf_append(buf, str, len);
	       str += len;
	       if (*str == 0)
		       break;
	       else if (str[1] == '[') {
		       char *q = strchr(str + 2, ']');
		       if (q) {
			       if (docmacro_expand(exp, str + 2, q - str - 2, buf))
				       textbuf_append(buf, str, q - str + 1);
			       str = q + 1;
		       } else {
			       textbuf_append(buf, str, 2);
			       str += 2;
		       }
	       } else {
		       textbuf_append_char(buf, str[1]);
		       str += 2;
	       }
       }
}


static int header_col = 1;
static int opt_col = 2;
static int opt_doc_col = 29;
#define DEFAULT_RIGHT_MARGIN 79
static int rmargin = DEFAULT_RIGHT_MARGIN;

static void
detect_right_margin(void)
{
	struct winsize ws;
	unsigned r = 0;

	ws.ws_col = ws.ws_row = 0;
	if ((ioctl (1, TIOCGWINSZ, (char *) &ws) < 0) || ws.ws_col == 0) {
		char *p = getenv ("COLUMNS");
		if (p) {
			unsigned long n;
			char *ep;
			errno = 0;
			n = strtoul (p, &ep, 10);
			if (!(errno || *ep || n > UINT_MAX))
				r = n;
		} else
			r = DEFAULT_RIGHT_MARGIN;
	} else
		r = ws.ws_col;
	rmargin = r;
}

static void
format_para(int left, int right, char const *text, struct docmacro *exp, FILE *fp)
{
	int width = right - left - 1;
	struct textbuf buf = TEXTBUF_INITIALIZER;

	if (strchr(text, '%')) {
		expand_vars(text, exp, &buf);
		textbuf_append_char(&buf, 0);
		text = textbuf_string(&buf);
	}

	if (strlen (text) < width)
		fprintf(fp, "%s\n", text);
	else {
		int len = 0;

		while (*text) {
			size_t n = strcspn(text, " \t\n");

			if (len + n > width) {
				fprintf(fp, "\n%*.*s", left, left, "");
				len = 0;
				while (*text && (*text == ' ' || *text == '\t'))
					;
				continue;
			} else if (text[n] == '\n') {
				len = 0;
				do {
					n++;
					len++;
				} while (text[n] &&
					 (text[n] == ' ' || text[n] == '\t'));
				len--;
			} else
				len += n;

			fwrite(text, n, 1, fp);
			text += n;

			while (*text && (*text == ' ' || *text == '\t'))
				text++;

			if (*text) {
				fputc (' ', fp);
				len++;
			}
		}
		fputc ('\n', fp);
	}
	textbuf_free(&buf);
}

static void
parseopt_list_options(struct parseopt *po, FILE *fp)
{
	struct optdef *opt;
	int prev_doc = 0;

	for (opt = po->optdef; opt->opt_name; ) {
		if (opt->opt_flags & OPTFLAG_DOC) {
			if (!prev_doc)
				fputc('\n', fp);
			fprintf(fp, "%*.*s", header_col, header_col, "");
			format_para(header_col, rmargin, opt->opt_doc,
				    po->docmacros, fp);
			fputc('\n', fp);
			opt++;
			prev_doc = 1;
		} else {
			struct optdef *cur_opt = opt;
			int col;

			col = fprintf(fp, "%*.*s-%s", opt_col, opt_col, "",
				      cur_opt->opt_name);
			for (opt++; opt->opt_name &&
				     (opt->opt_flags & OPTFLAG_ALIAS); opt++) {
				col += fprintf(fp, ", -%s", opt->opt_name);
			}
			if (cur_opt->opt_argdoc) {
				col += fprintf(fp, " %s", cur_opt->opt_argdoc);
			}
			if (col >= opt_doc_col) {
				fputc('\n', fp);
				col = 0;
			}
			for (; col < opt_doc_col; col++)
				fputc(' ', fp);
			format_para(opt_doc_col, rmargin, cur_opt->opt_doc,
				    po->docmacros, fp);
			prev_doc = 0;
		}
	}
}

int
parseopt_next(struct parseopt *po, char **ret_arg)
{
	int i;
	char *arg;
	struct optdef *opt;

	if (po->argi == po->argc)
		return EOF;

	if ((po->flags & OPTF_PERMUTE) && po->argi == po->argstart)
		return EOF;

	i = po->argi++;
	arg = po->argv[i];
	if (arg[0] == '-') {
		if (arg[1] == '-' && arg[2] == 0) {
			if (po->argi == po->argc)
				return EOF;
			*ret_arg = po->argv[po->argi++];
			return 0;
		}

		opt = option_find(po, arg + 1);
		if (!opt)
			exit(EX_USAGE);
		if (opt->opt_argdoc) {
			if (po->argi == po->argc) {
				error(EX_USAGE, 0,
				      "option '-%s' requires argument",
				      opt->opt_name);
			}
			*ret_arg = po->argv[po->argi++];
		}
		return opt->opt_code;
	} else if (strcmp(arg, "(") == 0 || strcmp(arg, ")") == 0) {
		return arg[0];
	} else {
		po->argi--;
		if ((po->flags & OPTF_PERMUTE) && po->argi + 1 < po->argc) {
			memmove(po->argv + i, po->argv + i + 1,
				(po->argc - i) * sizeof(po->argv[0]));
			po->argv[po->argc-1] = arg;
			if (po->argstart == 0)
				po->argstart = po->argc-1;
			else
				po->argstart--;
			return parseopt_next(po, ret_arg);
		}
	}
	*ret_arg = arg;
	return EOF;
}

int
parseopt_init(struct parseopt *po, int argc, char **argv)
{
	int i, j, k;

	if ((progname = strrchr(argv[0], '/')) == NULL)
		progname = argv[0];
	else
		progname++;

	po->argc = argc;
	po->argv = argv;
	po->argi = 1;
	po->argstart = 0;

	/* Collect and sort actual options */
	po->optcount = 0;
	for (i = 0; po->optdef[i].opt_name; i++)
		if (!(po->optdef[i].opt_flags & OPTFLAG_DOC))
			po->optcount++;

	if (po->optcount == 0)
		return 0;

	po->optidx = xcalloc(po->optcount, sizeof(po->optidx[0]));
	for (i = 0; po->optdef[i].opt_flags & OPTFLAG_DOC; i++)
		;

	j = 0;
	po->optidx[j++] = i;
	if (po->optcount > 1) {
		for (i++; po->optdef[i].opt_name; i++) {
			if (po->optdef[i].opt_flags & OPTFLAG_DOC)
				continue;
			po->optidx[j] = i;
			for (k = j;
			     k > 0 &&
			       strcmp(po->optdef[po->optidx[k-1]].opt_name,
				      po->optdef[po->optidx[k]].opt_name) > 0;
			     k--) {
				int tmp = po->optidx[k];
				po->optidx[k] = po->optidx[k-1];
				po->optidx[k-1] = tmp;
			}
			j++;
		}
	}
	return 0;
}

void
usage(int err, struct parseopt *po)
{
	FILE *fp = err ? stderr : stdout;
	detect_right_margin();
	fprintf(fp, "usage: %s %s\n", progname, po->argdoc);
	format_para(0, rmargin, po->descr, po->docmacros, fp);
	parseopt_list_options(po, fp);
	fputc('\n', fp);

	if (po->package_bugreport)
		fprintf(fp, "Report bugs and suggestions to <%s>\n",
			po->package_bugreport);
	if (po->package_url)
		fprintf(fp, "%s home page: <%s>\n",
			po->package_name ? po->package_name : po->argv[0],
			po->package_url);
	exit(err);
}

void
error(int ex, int err, char const *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	fprintf(stderr, "%s: ", progname);
	vfprintf(stderr, fmt, ap);
	if (err)
		fprintf(stderr, ": %s", strerror(err));
	fputc('\n', stderr);
	va_end(ap);
	if (ex)
		exit(ex);
}
